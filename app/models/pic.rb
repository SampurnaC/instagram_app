class Pic < ActiveRecord::Base
	belongs_to :user
	has_many :comments, dependent: :destroy
	mount_uploader :avatar, AvatarUploader
	has_many :likes
	belongs_to :category

	def self.search(search)
		# where("title LIKE ?", "%#{search}%")
		where(title: search)
	end
end
