$(document).ready(function() {
	$('#pic-like').click(function() {
		var pic_id = $('#pic-id').text();
		var user_id = $('#user-id').text();
		$.ajax('/pic/likes', {
			type: 'GET',
			data: { user_id: user_id, pic_id: pic_id },
			success: function(data) {
				var like_count = data['like_count']
				$('#pic-like').append(like_count);
			}
		})
	});
});