class PicsController < ApplicationController

  before_action :find_pics, only: [:show, :edit, :update, :destroy]

  def index

    if params[:search]
      @pics = Pic.search(params[:search]).page(params[:page]).per(3)
    else
      if params[:category].blank?
        @pics = Pic.all.order('created_at DESC').page(params[:page]).per(3)
      else
        @category_id = Category.find_by(name: params[:category]).id
        @pics = Pic.where(category_id: @category_id).order("created_at DESC").page(params[:page]).per(3)
      end
    end
  end

  def show
  end

  def new
    @pic = current_user.pics.build
  end

  def create
    @pic = current_user.pics.build(pic_params)
    if @pic.save
      redirect_to @pic, notice: "Yes, it was posted !"
    else
      render 'new'
    end
  end

  def update
    if @pic.update(pic_params)
      redirect_to @pic, notice: "Congrats! Pic was updated !"
    else
      render 'edit'
    end
  end

  def destroy
    @pic.destroy
    redirect_to pics_path
  end

  # def update_like
  #   user = params["user_id"].to_i
  #   pic = Pic.find params["pic_id"].to_i

  #   pic.like += 1
  #   pic.save

  #   respond_to do |format|
  #     format.json { render :json => { like_count: pic.like } }
  #   end
  # end


  private

  def pic_params
    params.require(:pic).permit(:title, :description, :avatar, :category_id)
  end

  def find_pics
    @pic = Pic.find(params[:id])
  end

end