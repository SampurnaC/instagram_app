class Pics::LikesController < ApplicationController 
	before_action :authenticate_user!
	before_action :set_post

	
	def create
		@pic.likes.where(user_id: current_user.id).first_or_create
		respond_to do |format|
			format.html { redirect_to @pic }
			format.js
		end
	end
  

  def destroy
  	@pic.likes.where(user_id: current_user.id).destroy_all
  	respond_to do |format|
			format.html { redirect_to @pic }
			format.js
		end
  end

	private

	  def set_post
		@pic = Pic.find(params[:pic_id])
		end
  
end

