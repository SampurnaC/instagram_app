Rails.application.routes.draw do

 root "pics#index"
 devise_for :users, controllers: { :omniauth_callbacks => "omniauth_callbacks" }
  resources :pics do
  	resource :like, module: :pics
  	resources :comments
  end


  # get '/pic/likes', to: 'pics#update_like'
  get '/about', to: 'pages#about'
  #get '/mail', to: 'pages#mail'

  # get '/auth/:provider/callback', to: 'sessions#create'

  # get 'auth/:provider/callback', to: 'sessions#create'


end