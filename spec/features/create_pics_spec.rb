require 'rails_helper'
include ActionView::Helpers::DateHelper

#visit root route
#click create post link
#fill in the form with needed information (title and description)
#click submit button
#expect page to have the content we submitted

feature 'pics' do
	describe 'Index' do
		before do
		 	@pic = Pic.make!
		 	visit root_path
		end

		 	it "should be on index path" do
				expect(current_path).to eq('/')
			end

			it "should have the right title" do
     		expect(page).to have_title("InstagramClone")
     	#expect(page.title).to include "InstagramClone"
    	end
			#click create post link
			it "should have the Join button" do
      	expect(page).to have_xpath("//a[@href='#{new_user_registration_path}']", text: "Join")
	    end

	    it "should have the Login button" do
	    	expect(page).to have_xpath("//a[@href='#{new_user_session_path}']", text: "Log In")
	    end

	    it "should have the navbar brand" do
        expect(page).to have_content('Instagram')
	    end
	    
	    it "should have the title and pic of the new created post" do
	    	expect(page).to have_xpath("//a[@href='/pics/#{@pic.id}']", text: @pic.title)
	    	expect(page).to have_xpath("//a[@href='/pics/#{@pic.id}']/img", visible: true)
	    end


	  	context "check for the categories" do
	  		before do
	  			@category = Category.make!
	  			@test_pic = Pic.make!(category_id: @category.id)

	  			visit root_path 
	  		end

		    it "should have the categories field" do
		    	expect(page).to have_xpath("//a[@href='/pics?category=#{@category.name}']", text: @category.name)
		  	end
	  	end
  end

  describe 'Show#GET' do
		before do
		 	user = User.make!
		 	@pic = Pic.make!(user: user)
		 	@comment = Comment.make!(pic_id: @pic.id)
	    
	    allow_any_instance_of(ApplicationController).to receive_messages(
	      authenticate_user!: true,
	      current_user: user
	    )

		 	visit pic_path(@pic)
		end
		
		it "should have the Edit button" do
    	expect(page).to have_xpath("//a[@href='#{edit_pic_path(@pic.id)}']", text: "Edit")
    end

    it "should have the Delete button" do
    	expect(page).to have_xpath("//a[@href='#{pic_path(@pic)}']", text: "Delete")
    end


    it "should have the title and pic of the clicked post" do
    	expect(page).to have_xpath("//h1", text: @pic.title)
    	expect(page).to have_xpath("//img[@src='#{@pic.avatar_url}']", visible: true)
    end

    it "should count the like" do
    	find(:xpath, "//a[@class='glyphicon glyphicon-heart-empty like-success']").click

    	expect(page).to have_xpath("//a[@class='glyphicon glyphicon-heart-empty like-unsuccess']")
    	expect(@pic.likes.count).to eq 1

    	find(:xpath, "//a[@class='glyphicon glyphicon-heart-empty like-unsuccess']").click
    	expect(@pic.likes.count).to eq 0
    	expect(page).to have_xpath("//a[@class='glyphicon glyphicon-heart-empty like-success']")
    end

    it "should have the text" do
     	expect(page).to have_text("by")
    end

    it "should have the email address of the user" do
    	expect(page).to have_xpath("//p[@class='user']", text: "by #{@pic.user.email} | Submitted at : #{time_ago_in_words(@pic.created_at)} ago")
    end

    it "should have the description of the post" do
    	expect(page).to have_xpath("//p", text: @pic.description)
    end

    it "should have the name of the commented person" do
     expect(page).to have_xpath("//div[@class='comment_content']/strong", text: "#{@comment.name}")
    end

    it "should have the description of the comment" do
    	expect(page).to have_xpath("//div[@class='comment_content']", text: "#{@comment.body}")
    end

    it "should have the date creation of comment" do
    	expect(page).to have_xpath("//div[@class='comment_content']/strong", text: "#{time_ago_in_words(@comment.created_at)} Ago")
    end

    it "should have the submit button" do
    	expect(page).to have_xpath("//button[@class='btn btn-info' and @id='comment-submit']", text: "submit")
    end

    it "should have the Delete button" do
    	# expect(page).to have_xpath("//a[@href='#{}'] and button[@class='btn btn-danger'] and @id='comment-delete'", text: "Delete")
    	expect(page).to have_xpath("//a[@href='/pics/#{@pic.id}/comments/#{@comment.id}' and @class='btn btn-danger' and @id='comment-delete']", text: "Delete")
    end

    it "should have the comment name form field" do
    	expect(page).to have_xpath("//input[@id='comment_name']")
    end

	end
end


