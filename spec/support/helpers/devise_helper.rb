module DeviseHelpers
  def emulate_user_login(user)
    allow_any_instance_of(ApplicationController).to receive_messages(
      authenticate_user!: true,
      current_user: user
    )
  end
end