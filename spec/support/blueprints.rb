require 'machinist/active_record'

# Add your blueprints here.
#
# e.g.
#   Post.blueprint do
#     title { "Post #{sn}" }
#     body  { "Lorem ipsum..." }
#   end

User.blueprint do
	email { "test-#{sn}@user.com" }
	password { "test@123" }
end

Pic.blueprint do
	title { Faker::GameOfThrones.character }
	avatar { File.open(File.join(Rails.root, '/spec/files/dell.png')) }
end

Category.blueprint do
	name { "laptops" }
end

Comment.blueprint do
	name { "Test comment" }
	body { "Test body" }
	bio { "Test bio" }
end