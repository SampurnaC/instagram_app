class AddBioToComments < ActiveRecord::Migration
  def change
    add_column :comments, :bio, :text
  end
end
