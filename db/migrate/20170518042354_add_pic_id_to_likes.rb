class AddPicIdToLikes < ActiveRecord::Migration
  def change
  	add_column :likes, :pic_id, :integer
  	add_index :likes, :pic_id
  end
end
